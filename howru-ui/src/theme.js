import { createTheme } from '@mui/material/styles';

export const theme = createTheme({
	status: {
		danger: '#e53e3e',
	},
	palette: {
		primary: {
			main: '#FF6058',
			contrastText: '#fff',
		},
	},
	components: {
		MuiCard: {
			styleOverrides: {
				root: {
					flexGrow: 1,
					borderRadius: 10,
				}
			}
		},
	}
});