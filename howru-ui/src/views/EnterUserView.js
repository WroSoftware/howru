import React, {useState} from 'react';
import styled from "styled-components";
import {Button, TextField} from "@mui/material";
import {useNavigate} from "react-router-dom";
import sessionService from "../services/service";

const Container = styled.div`
  display: flex;
  justify-content: center;
  align-content: center;
  align-items: center;
  height: 80vh;
`

const TextFieldContainer = styled.div`
  width: 500px;
  margin-right: 8px;
  margin-bottom: 5px;
`
const CustomTextField = styled(TextField)({
    height: '50px',
    width: '500px'
})

const CustomButton = styled(Button)({
    height: '50px',
})

const EnterUserView = () => {
    const navigate = useNavigate();
    const [userId, setUserId] = useState('');

    const onClick = () => sessionService.createUser(userId)
            .then(({ data }) => navigate('/ekran2', { state: { sessionId: data.sessionId } }));

    return <Container>
        <TextFieldContainer>
            <CustomTextField onChange={(e) => setUserId(e.target.value)} label="User identifier" />
        </TextFieldContainer>
        <CustomButton onClick={onClick} variant="contained" color="primary">
            WEJDZ >>>
        </CustomButton>
    </Container>
}


export default EnterUserView;