import CustomCard from '../../components/CustomCard';
import {Row} from "../../styles";
import EmotionItem from "../../components/EmotionItem";

const EmotionsRecognition = ({ statistics}) => {
    return (
        <CustomCard title={'Emotions recognition'}>
            <Row gap={48} grow={1} isCentered>
                <EmotionItem name={'Anger'} value={(statistics?.angry||0) * 100}/>
                <EmotionItem name={'Scared'} value={(statistics?.scared||0) * 100}/>
                <EmotionItem name={'Happy'} value={(statistics?.happy||0) * 100}/>
                <EmotionItem name={'Neutral'} value={(statistics?.neutral||0) * 100}/>
                <EmotionItem name={'Sad'} value={(statistics?.sad||0)}/>
            </Row>
        </CustomCard>
    );
};

export default EmotionsRecognition;
