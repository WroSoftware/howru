import React, {useEffect, useState} from "react";
import {useParams} from "react-router-dom";
import StudentInformation from "./StudentInformation";
import SessionTranscript from "./SessionTranscript";
import ActivityStats from "./ActivityStats";
import {Column, Row} from "../../styles";
import EmotionsRecognition from "./EmotionsRecognition";
import StatusPanel from "./StatusPanel";
import UnusualActivity from "./UnusualActivity";
import {useLocation} from "react-router";
import service from "../../services/service";

const SummaryView = () => {
    const params = useParams();
    const [sessionDetails, setSessionDetails] = useState('');

    const sessionId = parseInt(params.sessionId, 10);
    const { state } = useLocation();

    useEffect(() => {
        service.getSessionDetails(sessionId)
            .then(({ data }) => setSessionDetails(data))
    }, [])

    return (
        <Row>
            <Column grow={3}>
                <Row>
                    <StudentInformation image={sessionDetails.image} name={state.name} className={state.className}/>
                </Row>
                <Row>
                    <SessionTranscript transcription={sessionDetails.transcription}/>
                </Row>
            </Column>

            <Column grow={9}>
                <Row grow={1}>
                    <Row grow={7}>
                        <EmotionsRecognition statistics={sessionDetails.statistics}/>
                    </Row>
                    <Row grow={5}>
                        <StatusPanel/>
                    </Row>
                </Row>
                <UnusualActivity/>
                <ActivityStats/>
            </Column>
        </Row>
    );
}

export default SummaryView;