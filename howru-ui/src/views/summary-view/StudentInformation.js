import {Button, Card, CardMedia, Typography} from '@mui/material';
import CustomCard from '../../components/CustomCard';
import {Column, Row} from '../../styles';

const StudentInformation = ({ image, name, className }) => {
    console.log(image, 'image')

    return (
        <CustomCard elevation={2} title={'Student information'}>
            <Column gap={24}>
                <Column gap={18}>
                    <Card elevation={2}>
                        {image && <CardMedia
                            component={'img'}
                            height={160}
                            src={'data:image/jpeg;base64,' + image}
                        />}
                    </Card>
                    <Typography variant='body2' color={'text.secondary'}>
                        {name} | {className}<br/>
                        {name.replace(' ', '.').toLowerCase()}@school.com
                    </Typography>
                </Column>
                <Column gap={16}>
                    <Row grow={1} gap={24}>
                        <CustomButton>
                            Make note
                        </CustomButton>
                        <CustomButton>
                            Book meeting
                        </CustomButton>
                    </Row>
                    <Row grow={1} gap={24}>
                        <CustomButton>
                            Notify
                        </CustomButton>
                        <CustomButton>
                            History
                        </CustomButton>
                    </Row>
                </Column>
            </Column>
        </CustomCard>
    );
};

export default StudentInformation;

const CustomButton = ({children}) =>
    <Button variant={'contained'} style={{textTransform: 'capitalize'}} fullWidth>
        {children}
    </Button>
