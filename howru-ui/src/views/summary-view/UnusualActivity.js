import {TextField, Typography} from '@mui/material';
import CustomCard from '../../components/CustomCard';
import {Column, Row} from "../../styles";

const UnusualActivity = () => {
    const chunkified = chunk(data, 4);
    return (
        <Row>
            <CustomCard title={'Unusual activity'}>
                <Row>
                    {chunkified.map(chunk => (
                        <Column gap={0}>
                            {chunk.map(entry =>
                                <Typography variant={'body1'}>
                                    {entry}
                                </Typography>
                            )}
                        </Column>
                    ))}
                </Row>
            </CustomCard>
        </Row>
    );
};

export default UnusualActivity;

const chunk = (arr, size) =>
    Array.from({ length: Math.ceil(arr.length / size) }, (v, i) =>
        arr.slice(i * size, i * size + size)
    );

const data = [
    'Tired eyes',
    'Less motivated',
    'Less active',
    'Lower grades',
    'More aggresive',
    'Sadder',
    'Less present',
];