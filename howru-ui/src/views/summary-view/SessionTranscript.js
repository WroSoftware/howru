import {Box, TextField} from '@mui/material';
import CustomCard from '../../components/CustomCard';

const SessionTranscript = ({ transcription }) => {
    return (
        <CustomCard title={'Latest session transcript'}>
            <TextField
                multiline
                rows={12}
                fullWidth
                disabled
                value={transcription || 'There was no transcript for this session'}
            />
        </CustomCard>
    );
};

export default SessionTranscript;
