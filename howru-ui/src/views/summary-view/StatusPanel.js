import {Box, TextField, Typography} from '@mui/material';
import CustomCard from '../../components/CustomCard';
import {Row} from "../../styles";

const StatusPanel = () => {
    return (
        <CustomCard title={'Status'}>
            <Typography variant={'h4'} color={'#FF7170'} fontWeight={700}>
                <Row isCentered>
                    ACTION NEEDED
                </Row>
            </Typography>
        </CustomCard>
    );
};

export default StatusPanel;
