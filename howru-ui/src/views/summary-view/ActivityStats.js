import ExampleChart from "../../components/ExampleChart";
import React from "react";
import CustomCard from "../../components/CustomCard";

const ActivityStats = () => {
    return (
        <CustomCard title={'Attendance & Activity statistics'}>
            <ExampleChart/>
        </CustomCard>
    );
};

export default ActivityStats;
