import React, {useEffect, useState} from "react";
import {Button, Table, TableBody, TableCell, TableHead, TableRow, Typography} from "@mui/material";
import styled from 'styled-components';
import {useNavigate} from "react-router-dom";
import {TableBox, TableHeader} from "./NotificationsView";
import {useLocation} from "react-router";
import service from "../services/service";

const Container = styled.div`
  width: 1200px;
  margin: auto;
`

const rows = [
    {
        sessionId: 1,
        start: new Date().toDateString(),
        end: new Date().toDateString(),
        amount: 2
    },
    {
        sessionId: 2,
        start: new Date().toDateString(),
        end: new Date().toDateString(),
        amount: 4
    },
    {
        sessionId: 3,
        start: new Date().toDateString(),
        end: new Date().toDateString(),
        amount: 10
    },
    {
        sessionId: 4,
        start: new Date().toDateString(),
        end: new Date().toDateString(),
        amount: 30
    }
];

const SessionHistoryView = () => {

    const [sessions, setSessions] = useState([]);
    const navigate = useNavigate();
    const { state } = useLocation();
    console.log(state)


    useEffect(() => {
        service.getAllSessionsByUserId(state.userId)
            .then(({ data }) => setSessions(data))
    }, [])

    return <Container>
        <TableHeader>
            <Typography variant="h5" component="div">
                Sessions for: {state.name}
            </Typography>
        </TableHeader>
        <TableBox>
            <Table sx={{ width: '100%' }} aria-label="simple table">
                <TableHead>
                    <TableRow>
                        <TableCell align={'center'}><b>Session number</b></TableCell>
                        <TableCell align={'center'}><b>Start at</b></TableCell>
                        <TableCell align={'center'}><b>End at</b></TableCell>
                        <TableCell align={'center'}><b>People in meeting</b></TableCell>
                        <TableCell align={'center'}></TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {sessions.map((row) => (
                        <TableRow
                            key={row.id}
                            sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                        >
                            <TableCell align={'center'}>{row.id}</TableCell>
                            <TableCell align={'center'}>{row.start}</TableCell>
                            <TableCell align={'center'}>{row.end}</TableCell>
                            <TableCell align={'center'}>{row.persons}</TableCell>
                            <TableCell align={'center'}>
                                <Button
                                    onClick={() => navigate(`/sessions/${row.id}`, { state })}
                                    color='primary'
                                    variant="contained"
                                >
                                    Inspect
                                </Button>
                            </TableCell>
                        </TableRow>
                    ))}
                </TableBody>
            </Table>
        </TableBox>
    </Container>
}

export default SessionHistoryView;