import React, {useEffect, useState} from "react";
import {
    Avatar,
    Container,
    Grid, Paper, Table, TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow,
    Typography
} from "@mui/material";
import styled from "styled-components";
import {Hover} from "../styles";
import {useNavigate} from "react-router-dom";
import service from "../services/service";

function createData(id, name, className, teacher, status) {
    return { id, name, className, teacher, status };
}

const getRandom = (arr) => arr[Math.floor(Math.random() * arr.length)]
const name = ['Alan', 'Kara', 'Sławek', 'Iza', 'Jarek', 'Wiktor', 'Seba', 'Maciek'];
const classes = ['Sporty Class', 'Beauty Class', 'Best Class', 'Sad Class', 'XD']
const teachers = ['Pinokio', 'Kubuś Puchatek', 'Mały Książe']
const surename = [
'Nowak',
'Jezieski',
'Herbut',
'Babis',
'Konradzki',
'Godyń',
'Kuras',
'Dziadyk',
'Patrzek',
'Skoropada',
'Kańka',
]

export const TableBox = styled.div`
  background-color: rgba(250, 250, 250, 0.8);
  border: 1px solid black;
  border-radius: 16px;
  margin: 32px auto;
  font-family: "Roboto","Helvetica","Arial",sans-serif;
  font-size: 0.875rem;
`

export const TableHeader = styled.div`
  margin: 32px 32px;
`;

const NotificationView = () => {
    const navigate = useNavigate()

    const [rows, setRows] = useState([]);

    useEffect(() => {
        service.getAllUsers()
            .then(({ data }) => {
                console.log(data, 'data')
                setRows(data.map((u, idx) => ({
                    id: u.id,
                    name: u.name + ' ' + u.surname,
                    className: 'Class ' + u.className, teacher: u.teacherName + ' ' + u.teacherSurname,
                    status: u.status ? 'EVERYTHING OK' : 'ACTION_NEEDED'})))
            })
    }, []);



    return <Grid container rowSpacing={2} spacing={2}>
        <TableBox>
            <TableHeader>
                <Typography variant="h5" component="div">
                   Recent notifications
                </Typography>
            </TableHeader>
            <Container maxWidth="lg">
                <TableContainer>
                    <Table stickyHeader sx={{ width: '1000px' }} aria-label="simple table">
                        <TableHead>
                            <TableRow>
                                <TableCell>Name</TableCell>
                                <TableCell></TableCell>
                                <TableCell align="right">Class</TableCell>
                                <TableCell align="right">Supervising Teacher</TableCell>
                                <TableCell align="right">Status</TableCell>
                                <TableCell align="right"></TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {rows.map((row) => (
                                    <TableRow hover onClick={() => navigate(`/user/${row.id}/sessions`, { state: { name: row.name, userId: row.id, className: row.className }})} key={row.name}>
                                        <TableCell>
                                            <Avatar>
                                                {row.name[0]}
                                            </Avatar>
                                        </TableCell>
                                        <TableCell align="right">{row.name}</TableCell>
                                        <TableCell align="right">{row.className}</TableCell>
                                        <TableCell align="right">{row.teacher}</TableCell>
                                        <TableCell align="right">{row.status}</TableCell>
                                        <TableCell align="right">
                                                ➜
                                        </TableCell>
                                    </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                </TableContainer>
            </Container>
        </TableBox>
    </Grid>
}

export default NotificationView;