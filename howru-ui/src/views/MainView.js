import React from 'react';
import {Link} from 'react-router-dom';
import {Box, Container} from "@mui/material";


const MainView = () => {
    return (
        <div>
            <div>
                <Link to='/ekran1'>ekran1</Link>
            </div>
            <div>
                <Link to='/login'>login</Link>
            </div>
            <div>
                <Link to='/dashboard'>dashboard</Link>
            </div>
            <div>
                <Link to='/notifications'>notifications</Link>
            </div>
            <div>
                <Link to='/ekran2'>ekran2</Link>
            </div>
            <div>
                <Link to='/ekran3'>ekran3</Link>
            </div>
            <div>
                <Link to='/sessions/1'>ekran4</Link>
            </div>
        </div>
    );
}

export default MainView;