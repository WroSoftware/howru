import React from "react";
import Webcam from "react-webcam";
import styled from 'styled-components';
import SpeechToText from "../components/SpeechToText";
import useInterval from "../hooks/useInterval";
import {BASE_URL} from "../services/service";
import background from '../resource/img/img.png';
import {useLocation, useParams} from "react-router";

const Container = styled.div`
  display: flex;
  width: 100%;
  height: 100vh;
  background-image: url(${background});
  background-repeat: no-repeat;
  background-position: center;
  background-size: 1500px;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  align-content: center;
`

const WebCamContainer = styled.div`
  position: absolute;
  
  display: flex;
  height: 250px;
  top: 85px;
  left: 285px;
`

const TopPanel = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  margin: 8px 0;
  width: 520px;
`

const SpeechContainer = styled.div`
  position: absolute;
  width: 350px;
  overflow: hidden;
  top: 100px;
  left: 1360px;
`

const SessionView = () => {


    const [capturing, setCapturing] = React.useState(false);
    const { state } = useLocation();
    console.log(state)
    const [text, setText] = React.useState('')
    const webcamRef = React.useRef(null);
    const mediaRecorderRef = React.useRef(null);

    // useEffect(() => {
    //     if(capturing) {
    //         mediaRecorderRef.current = new MediaRecorder(webcamRef.current.stream, {
    //             mimeType: "video/webm"
    //         });
    //         mediaRecorderRef.current.addEventListener(
    //             "dataavailable",
    //             handleDataAvailable
    //         );
    //         mediaRecorderRef.current.start(1000);
    //     }
    // }, [capturing])

    useInterval(sendScreenShoot, 5000)

    function sendScreenShoot() {
        const imageSrc = webcamRef.current.getScreenshot();

        let headers = new Headers();


        fetch(
            BASE_URL + `/api/session/${state.sessionId}/screenshot`,
            {
                mode: 'cors',
                method: 'POST',
                headers: {
                    ...headers,
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    image: imageSrc,
                    transcription: text,
                    sessionId: state.sessionId
                }),
            },
        ).catch(console.log)
    };


    const handleDataAvailable = ({ data }) => {
        data.arrayBuffer().then(console.log)
    }

    console.log('rere', text)

    return <div style={{ backgroundColor: '#222' }}>
        <Container>

            {/*<button onClick={() => setCapturing(!capturing)}>Stop Capture</button>*/}
            <WebCamContainer>
                <Webcam screenshotFormat={'image/jpeg'} audio={false} ref={webcamRef} />
            </WebCamContainer>
            <SpeechContainer>
                <SpeechToText saveTranscription={setText} />
            </SpeechContainer>
        </Container>
    </div>
}

export default SessionView;