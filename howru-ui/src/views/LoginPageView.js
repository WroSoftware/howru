import React, {useState} from "react";
import styled from "styled-components";
import background from '../resource/img/bg_logo_page2.jpg';
import {Button, TextField} from "@mui/material";
import {useNavigate} from "react-router-dom";

const Container = styled.div`
  display: flex;
  justify-content: center;
  background-image: url(${background});
  width: 100vw;
  height: 100vh;
`;

const Form = styled.div`
  display: flex;
  margin-top: 15%;
  background: rgba(250, 250, 250, 0.7);
  justify-content: center;
  flex-direction: column;
  border: 1px solid rgba(0.3, 0.3, 0.3, 0.3);
  border-radius: 10px;
  width: 20%;
  height: 20%;
  padding: 64px 32px;
`;

const TextFieldContainer = styled.div`
  margin: 8px;
`;

const Header = styled.div`
  display: flex;
  justify-content: center;
  color: #555;
  font-size: 48px;
  font-weight: bold;
  margin: 16px;
`;

const ButtonContainer = styled.div`
  display: flex;
  justify-content: center;
  margin: 8px 0;
`;

const LoginPageView = () => {

    const navigate = useNavigate();

    function handleSubmit(event) {
        event.preventDefault();
    }


    return <Container>
        <Form>
            <Header>
                MTE
            </Header>
            <TextFieldContainer>
                <TextField fullWidth id="outlined-basic" label="name" variant="outlined" />
            </TextFieldContainer>
            <TextFieldContainer>
                <TextField fullWidth id="outlined-basic" label="password" variant="outlined" />
            </TextFieldContainer>

            <ButtonContainer>
                <Button onClick={() => navigate('/ekran2')} size={'medium'} variant="contained" color="primary">
                    Login
                </Button>
            </ButtonContainer>
        </Form>
    </Container>


}


export default LoginPageView;