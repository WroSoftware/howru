import React, {useEffect, useState} from 'react';
import {Alert, AlertTitle, Button, Typography} from '@mui/material';
import {Column, Row} from '../styles';
import CustomCard from '../components/CustomCard';
import ExampleChart from '../components/ExampleChart';
import {Link, useNavigate} from 'react-router-dom';
import StudentsList from '../components/StudentsList';
import service from "../services/service";


const DashboardView = () => {

    const navigate = useNavigate();
    const [usersAmount, setUsersAmount] = useState([]);

    useEffect(() => {
        service.getAllUsers()
            .then(({ data }) => {
                setUsersAmount(data.length);
            })
    }, []);

    return (
        <Column>
            <CustomCard title={'Hi Psychologist!'}>
                <Typography variant='body1' component='div'>
                    {usersAmount&&<Alert severity='error'>
                        <AlertTitle>Attention</AlertTitle>
                        You have <strong>{usersAmount}</strong> cases that require your attention —
                        <Button
                            sx={{color: 'inherit', fontWeight: 700, textTransform: 'lowercase'}}
                            onClick={() => navigate('/notifications')}
                        >check it out
                        </Button>
                    </Alert>}
                </Typography>
            </CustomCard>

            <Row grow={1}>
                <Row grow={5}>
                    <CustomCard title={'Last seen'}>
                        <StudentsList data={students}/>
                    </CustomCard>
                </Row>
                <Row grow={3}>
                    <CustomCard title={'Statistics'}>
                        <ExampleChart/>
                    </CustomCard>
                </Row>
            </Row>
        </Column>
    );
}

export default DashboardView;

const students = [
    {
        name: 'Ali Connors',
        group: 'IIA',
    },
    {
        name: 'Sandra Adams',
        group: 'IB',
    },
    {
        name: 'Jennifer Grey',
        group: 'IIC',
    },
];
