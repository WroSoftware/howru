import React from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter, Route, Routes} from 'react-router-dom';
import EnterUserView from './views/EnterUserView';
import {Box, Container, Paper, ThemeProvider} from '@mui/material';
import {theme} from './theme';
import SessionView from './views/SessionView';
import SessionHistoryView from './views/SessionHistoryView';
import SummaryView from './views/summary-view/SummaryView';
import LoginPageView from './views/LoginPageView';
import DashboardView from './views/DashboardView';
import NotificationsView from './views/NotificationsView';
import NavigationBar from './components/NavigationBar';
import {Row} from './styles';

ReactDOM.render(
  <React.StrictMode>
      <ThemeProvider theme={theme}>
          <BrowserRouter>
              <Paper elevation={1} sx={{margin: '8px'}}>
                  <Row grow={1} gap={0}>
                      <NavigationBar/>
                      <Box backgroundColor={'#f8f7fc'} height={'calc(100vh - 16px)'} flexGrow={1}>
                          <Container maxWidth={'xl'} style={{padding: 32}}>
                                  <Routes>
                                      <Route path='/' element={<DashboardView />} />
                                      <Route path='/login' element={<LoginPageView />} />
                                      <Route path='/notifications' element={<NotificationsView />} />
                                      <Route path='ekran1' element={<EnterUserView />} />
                                      <Route path='ekran2' element={<SessionView />} />
                                      <Route path='user/:userId/sessions' element={<SessionHistoryView />} />
                                      <Route path='sessions/:sessionId' element={<SummaryView />} />
                                  </Routes>
                          </Container>
                      </Box>
                  </Row>
              </Paper>
          </BrowserRouter>
      </ThemeProvider>
  </React.StrictMode>,
  document.getElementById('root')
);
