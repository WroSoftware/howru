import React from 'react';
import Webcam from 'react-webcam';

function App() {

  const webcamRef = React.useRef(null);
  const mediaRecorderRef = React.useRef(null);
  const [capturing, setCapturing] = React.useState(false);
  const [recordedChunks, setRecordedChunks] = React.useState([]);
  const [urlToSend, setUrlToSend] = React.useState('');

  const handleStartCaptureClick = React.useCallback(() => {
    setCapturing(true);
    mediaRecorderRef.current = new MediaRecorder(webcamRef.current.stream, {
      mimeType: "video/webm"
    });
    mediaRecorderRef.current.addEventListener(
      "dataavailable",
      handleDataAvailable
    );
    mediaRecorderRef.current.start();
  }, [webcamRef, setCapturing, mediaRecorderRef]);

  const handleDataAvailable = React.useCallback(
    ({ data }) => {
      if (data.size > 0) {
        setRecordedChunks((prev) => prev.concat(data));
      }
    },
    [setRecordedChunks]
  );

  const sendData = () => {
    fetch(
      urlToSend,
      {
        method: 'PUT',
        body: recordedChunks[0],
        headers: {
          'Content-Type': 'video/webm',
          'x-amz-acl': 'public-read',
        },
      },
    ).catch(console.log)
  }

  const sendScreenShoot = () => {
    const imageSrc = webcamRef.current.getScreenshot();
    console.log(imageSrc, 'ge')
    fetch(
      urlToSend,
      {
        method: 'PUT',
        body: imageSrc,
        headers: {
          'Content-Type': 'image/jpeg',
        },
      },
    ).catch(console.log)
  }

  const handleStopCaptureClick = React.useCallback(() => {
    mediaRecorderRef.current.stop();
    setCapturing(false);
  }, [mediaRecorderRef, webcamRef, setCapturing]);

  const handleDownload = React.useCallback(() => {
    if (recordedChunks.length) {
      const blob = new Blob(recordedChunks, {
        type: "video/webm"
      });
      const url = URL.createObjectURL(blob);
      const a = document.createElement("a");
      document.body.appendChild(a);
      a.style = "display: none";
      a.href = url;
      a.download = "react-webcam-stream-capture.webm";
      a.click();
      window.URL.revokeObjectURL(url);
      setRecordedChunks([]);
    }
  }, [recordedChunks]);
  console.log(recordedChunks);
  return (
    <>
      <Webcam audio={false} ref={webcamRef} />
      {capturing ? (
        <button onClick={handleStopCaptureClick}>Stop Capture</button>
      ) : (
        <button onClick={handleStartCaptureClick}>Start Capture</button>
      )}
        <div>Recorded chunks size: {recordedChunks.length}</div>
        <label style={{ display: 'flex', flexDirection: 'column', marginBottom: '8px'}}>
          Type URL where blob will be send

          <input onChange={(v) => setUrlToSend(v.target.value)} value={urlToSend} style={{ width: '250px' }}  />
        </label>
        <div style={{ display: 'flex', flexDirection: 'column', width: '250px', height: '100px', justifyContent: 'space-between'}}>
          <button onClick={sendData}>Send</button>
          <button onClick={sendScreenShoot}>take and send screenshoot</button>
          <button onClick={handleDownload}>Download</button>
        </div>
    </>)
}

export default App;
