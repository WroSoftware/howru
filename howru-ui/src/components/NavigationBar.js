import React, {useState} from 'react';
import {Button, Paper, Tab, Tabs} from '@mui/material';
import HomeIcon from '@mui/icons-material/Home';
import NotificationsIcon from '@mui/icons-material/Notifications';
import PersonSearchIcon from '@mui/icons-material/PersonSearch';
import SettingsIcon from '@mui/icons-material/Settings';
import AppLogo from './AppLogo';
import {Column} from '../styles';
import ArrowRightIcon from '@mui/icons-material/ArrowRight';
import {Link} from 'react-router-dom';


const NavigationBar = () => {
    const [tab, setTab] = useState(0);

    const handleChange = (event, value) => {
        setTab(value);
    };

    return (
        <Paper elevation={3} style={{zIndex: 2}}>
            <Column gap={0} grow={1} isSpaced>
                <Column gap={12}>
                    <AppLogo/>
                    <Tabs
                        value={tab}
                        onChange={handleChange}
                        orientation={'vertical'}
                        sx={tabContainerStyle}
                        aria-label='icon position tabs example'
                    >
                        <LinkTab to={'/'} icon={<HomeIcon/>} iconPosition={'start'} label='Home'/>
                        <LinkTab to={'/notifications'} icon={<NotificationsIcon/>} iconPosition='start' label='Notifications'/>
                        <LinkTab to={'/ekran1'} icon={<PersonSearchIcon/>} iconPosition='start' label='Browse'/>
                        <LinkTab to={'/ekran1'} icon={<SettingsIcon/>} iconPosition='start' label='Settings'/>
                    </Tabs>
                </Column>
                <Button
                    endIcon={<ArrowRightIcon/>}
                    variant='contained'
                    sx={{textTransform: 'lowercase'}}
                >
                    psychologist@school.com
                </Button>
            </Column>
        </Paper>
    );
}

export default NavigationBar;

const LinkTab = (props) => {
    return (
        <Tab
            component={Link}
            sx={tabStyle}
            {...props}
        />
    );
}

const tabStyle = {
    justifyContent: 'start',
    minHeight: 56,
    '&.Mui-selected': {
        boxShadow: '32px 0 20px -12px #ff6058;',
    }
};

const tabContainerStyle = {
    '.MuiTabs-flexContainer': {
        padding: '12px 0 12px 0',
    }
}