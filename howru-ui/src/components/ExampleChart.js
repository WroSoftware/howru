import React, { PureComponent } from 'react';
import { AreaChart, Label, Area, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer } from 'recharts';

const addRandomNumberRelativeToArg = (arg) => {
    const scale = (Math.random() -0.5) * arg;
    return arg + scale;
}

const data = [
    {
        name: 'January',
        uv: addRandomNumberRelativeToArg(40),
        pv: addRandomNumberRelativeToArg(24),
        amt: addRandomNumberRelativeToArg(24),
    },
    {
        name: 'February',
        uv: addRandomNumberRelativeToArg(30),
        pv: addRandomNumberRelativeToArg(13),
        amt: addRandomNumberRelativeToArg(22),
    },
    {
        name: 'March',
        uv: addRandomNumberRelativeToArg(20),
        pv: addRandomNumberRelativeToArg(98),
        amt: addRandomNumberRelativeToArg(22),
    },
    {
        name: 'April',
        uv: addRandomNumberRelativeToArg(27),
        pv: addRandomNumberRelativeToArg(39),
        amt: addRandomNumberRelativeToArg(20),
    },
    {
        name: 'May',
        uv: addRandomNumberRelativeToArg(18),
        pv: addRandomNumberRelativeToArg(48),
        amt: addRandomNumberRelativeToArg(21),
    },
    {
        name: 'June',
        uv: addRandomNumberRelativeToArg(23),
        pv: addRandomNumberRelativeToArg(38),
        amt: addRandomNumberRelativeToArg(25),
    },
    {
        name: 'July',
        uv: addRandomNumberRelativeToArg(34),
        pv: addRandomNumberRelativeToArg(43),
        amt: addRandomNumberRelativeToArg(21),
    },
];

export default class ExampleChart extends PureComponent {
    static demoUrl = 'https://codesandbox.io/s/simple-line-chart-kec3v';

    render() {
        return (
            <div style={{fontFamily: '"Roboto","Helvetica","Arial",sans-serif', fontSize: '0.875rem'}}>
                <ResponsiveContainer width={'90%'} height={280}>
                    <AreaChart height={250} data={data}
                               margin={{ top: 10, right: 30, left: 0, bottom: 0 }}>
                        <defs>
                            <linearGradient id="colorUv" x1="0" y1="0" x2="0" y2="1">
                                <stop offset="5%" stopColor="#FF7170" stopOpacity={0.8}/>
                                <stop offset="95%" stopColor="#FF7170" stopOpacity={0}/>
                            </linearGradient>
                            <linearGradient id="colorPv" x1="0" y1="0" x2="0" y2="1">
                                <stop offset="5%" stopColor="#82ca9d" stopOpacity={0.8}/>
                                <stop offset="95%" stopColor="#82ca9d" stopOpacity={0}/>
                            </linearGradient>
                        </defs>
                        <XAxis dataKey="name" label={<span style={{color: 'red'}}/>}/>
                        <YAxis unit={'%'}/>
                        <CartesianGrid strokeDasharray="8 2" />
                        <Tooltip />
                        <Area type="monotone" dataKey="uv" stroke="#FF7170" fillOpacity={1} fill="url(#colorUv)" />
                        <Area type="monotone" dataKey="pv" stroke="#82ca9d" fillOpacity={1} fill="url(#colorPv)" />
                    </AreaChart>
                </ResponsiveContainer>
            </div>
        );
    }
}
