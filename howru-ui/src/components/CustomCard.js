import {Card, Typography} from '@mui/material';
import styled from 'styled-components';
import {Column} from "../styles";

const CustomCard = ({children, title}) => (
    <Card elevation={2}>
        <Padding>
            <Column gap={16}>
                <Typography variant='h5' component='h2'>
                    {title}
                </Typography>
                {children}
            </Column>
        </Padding>
    </Card>
);

export default CustomCard;

const Padding = styled.div`
    padding: 16px 24px 24px 24px;
`;
