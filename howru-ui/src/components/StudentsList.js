import * as React from 'react';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import Divider from '@mui/material/Divider';
import ListItemText from '@mui/material/ListItemText';
import ListItemAvatar from '@mui/material/ListItemAvatar';
import Avatar from '@mui/material/Avatar';
import Typography from '@mui/material/Typography';
import {stringAvatar} from "../avatarUtils";


const StudentsList = ({data}) => {
    return (
        <List sx={{ width: '100%', bgcolor: 'background.paper' }}>
            {data.map((student, idx) => (
                <>
                    {idx > 0 ? <Divider variant='inset' component='li' /> : null}
                    <ListItem alignItems='flex-start'>
                        <ListItemAvatar>
                            <Avatar {...stringAvatar(student.name)}/>
                        </ListItemAvatar>
                        <ListItemText
                            primary={student.name}
                            secondary={
                                <React.Fragment>
                                    <Typography
                                        sx={{ display: 'inline' }}
                                        component='span'
                                        variant='body2'
                                        color='text.secondary'
                                    >
                                        {student.group}
                                    </Typography>
                                </React.Fragment>
                            }
                        />
                    </ListItem>
                </>
            ))}
        </List>
    );
};

export default StudentsList;
