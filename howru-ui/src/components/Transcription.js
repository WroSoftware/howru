import React from "react";
import styled from "styled-components";

const TranscriptionContainer = styled.div`
  width: 100%;
  padding: 8px;
  margin: 0 4px;
  font-size: 16px;
`

const Transcription = ({ transcript }) => {
    return <TranscriptionContainer>
        <p style={{ fontSize: '18px' }}>{transcript}</p>
    </TranscriptionContainer>
}

export default Transcription;