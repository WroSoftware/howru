import {Column} from "../styles";
import {Avatar, Typography} from "@mui/material";

const EmotionItem = ({name, value}) => (
    <Column isCentered gap={12}>
        <Typography variant={'body1'}>
            {name}
        </Typography>
        <Avatar sx={{bgcolor: getColor(value), fontSize: '0.9rem'}}>
            {value}%
        </Avatar>
    </Column>
);

export default EmotionItem;

const getColor = (value) =>
    value < 25
        ? '#FF7170'
        : '#82ca9d';
