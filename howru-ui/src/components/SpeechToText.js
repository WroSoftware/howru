import React, {useEffect} from 'react';
import SpeechRecognition, { useSpeechRecognition } from 'react-speech-recognition';
import styled from 'styled-components';
import Transcription from "./Transcription";


const Header = styled.div`
  width: 536px;
`;

const SpeechToText = ({ saveTranscription }) => {

    const {
        transcript,
        browserSupportsSpeechRecognition,
    } = useSpeechRecognition()

    useEffect(() => {
        SpeechRecognition.startListening({ continuous: true });
    }, [])

    useEffect(() => {
        // console.log('save', transcript, saveTranscription !== undefined)
        saveTranscription !== undefined && saveTranscription(transcript);
    }, [transcript])

    if (!browserSupportsSpeechRecognition) {
        return <span>Browser doesn't support speech recognition.</span>;
    }

    return <Transcription transcript={transcript} />;
}

export default SpeechToText;