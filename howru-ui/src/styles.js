import styled from "styled-components";

export const Dot = styled.div`
  width: 32px;
  height: 32px;
  background-color: ${(props) => props.color};
  border-radius: 50%;
  margin: 4px;
`

export const Item = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: ${props => props.justifyContent || 'center'};
  align-items: center;
  margin: ${props => props.margin || '0'};
`

export const Row = styled.div`
    display: flex;
    gap: ${p => p.gap ?? 24}px;
    flex-grow: ${p => p.grow ?? 0};
    justify-content: ${p => p.isCentered && 'center'}
`;

export const Column = styled(Row)`
    flex-direction: column;
    align-items: ${p => p.isCentered && 'center'};
    justify-content: ${p => p.isSpaced && 'space-between'};
    height: 100%;
`;

export const Hover = styled.div`
  &:hover {
    cursor: pointer;
  }
`;
