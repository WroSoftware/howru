import axios from "axios";

export const BASE_URL = 'https://agile-fortress-78403.herokuapp.com';

const createUser = (userId) => {
    return axios.post(BASE_URL + `/api/user/${userId}/session`)
}

const getAllUsers = () => {
    return axios.get(BASE_URL + '/api/user')
}

const getAllSessionsByUserId = (userId) => {
    return axios.get(BASE_URL + `/api/user/${userId}/session`)
}

const getSessionDetails = (sessionId) => {
    return axios.get(BASE_URL + `/api/session/${sessionId}`)
}

export default {
    createUser,
    getAllUsers,
    getAllSessionsByUserId,
    getSessionDetails
}