package com.hackyeah.ruok.application;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/application")
class ApplicationController {

    @Value("${application.version}")
    private String version;

    @GetMapping("/version")
    public String getVersion() {
        return version;
    }
}
