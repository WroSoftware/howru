package com.hackyeah.ruok.session.model.entity;

import com.hackyeah.ruok.shared.model.entity.BaseEntity;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

@Data
@Entity
@SequenceGenerator(name = "gen", sequenceName = "image_anal_stat_gen", initialValue = 1, allocationSize = 1)
public class ImageAnalysisStatistic extends BaseEntity {

    private int angry = 0;
    private int scared = 0;
    private int happy = 0;
    private int sad = 0;
    private int neutral = 0;

    @ManyToOne(fetch = FetchType.LAZY)
    private Session session;

    public int iAmAngry() {
        return ++angry;
    }

    public int iAmScared() {
        return ++scared;
    }

    public int iAmHappy() {
        return ++happy;
    }

    public int iAmSad() {
        return ++sad;
    }

    public int iAmNeutral() {
        return ++neutral;
    }
}
