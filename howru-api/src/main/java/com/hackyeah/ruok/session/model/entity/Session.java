package com.hackyeah.ruok.session.model.entity;

import com.hackyeah.ruok.shared.model.entity.BaseEntity;
import com.hackyeah.ruok.user.model.entity.User;
import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import java.time.LocalDateTime;

@Data
@Entity
@SequenceGenerator(name = "gen", sequenceName = "session_gen", initialValue = 1, allocationSize = 1)
public class Session extends BaseEntity {

    private String name;
    @ManyToOne
    private User user;
    @UpdateTimestamp
    private LocalDateTime lastUpdate;
    private Integer persons;
    @Type(type="text")
    private String image;
}
