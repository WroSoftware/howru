package com.hackyeah.ruok.session.domain;

import com.hackyeah.ruok.analysis.model.dto.ImageAnalysisResponseDTO;
import com.hackyeah.ruok.analysis.model.dto.TextAnalysisRequestDTO;
import com.hackyeah.ruok.analysis.model.dto.TextAnalysisResponseDTO;
import com.hackyeah.ruok.session.model.dto.ScreenShotWithTranscriptionDTO;
import com.hackyeah.ruok.session.model.dto.SessionDTO;
import com.hackyeah.ruok.session.model.entity.*;
import com.hackyeah.ruok.user.model.dto.SessionForUserDTO;
import com.hackyeah.ruok.user.model.entity.User;
import lombok.AllArgsConstructor;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.stream.Collectors;

@Service
@Transactional
@AllArgsConstructor
class SessionService {

    private static Random RANDOM = new Random(System.currentTimeMillis());
    private final SessionRepository sessionRepository;
    private final TranscriptionRepository transcriptionRepository;
    private final TextAnalysisRepository textAnalysisRepository;
    private final ImageAnalysisStatisticRepository imageAnalysisStatisticRepository;
    private final SessionMapper sessionMapper;
    private final TextAnalysisMapper textAnalysisMapper;
    private final ImageAnalysisStatisticMapper imageAnalysisStatisticMapper;

    public SessionDTO createNewSession(User user) {
        Session session = createSession(user);
        createTranscription(session);
        createImageStatistics(session);
        return sessionMapper.mapToDto(session);
    }

    @Cacheable(key = "#sessionId")
    public Session getSession(Long sessionId) {
        return sessionRepository.findById(sessionId).orElse(null);
    }

    public void addScreenShotWithTranscription(Long sessionId, ScreenShotWithTranscriptionDTO dto) {
        Optional.ofNullable(dto)
                .map(ScreenShotWithTranscriptionDTO::getTranscription)
                .ifPresent(transcription -> transcriptionRepository.updateTranscription(sessionId, transcription));
    }

    public void addImageAnalysis(Session session, ImageAnalysisResponseDTO imageAnalysed) {
        ImageAnalysisStatistic imageAnalysisStatistic = imageAnalysisStatisticRepository.findBySessionId(session.getId());
        imageAnalysisStatisticMapper.recalculate(imageAnalysisStatistic, imageAnalysed);
        updateSession(session, imageAnalysed);
    }

    public void addTextAnalysis(Session session, TextAnalysisResponseDTO textAnalysed) {
        TextAnalysis textAnalysis = textAnalysisMapper.mapToEntity(textAnalysed, session);
        textAnalysisRepository.save(textAnalysis);
    }

    public List<SessionForUserDTO> getSessionsForUser(Long userId) {
        return sessionRepository.findAllByUserId(userId)
                .stream()
                .map(sessionMapper::mapToUserSession)
                .collect(Collectors.toList());
    }

    public SessionDetailsDTO getSessionDetails(Long sessionId) {
        Transcription transcription = transcriptionRepository.findBySessionId(sessionId);
        ImageAnalysisStatistic statistic = imageAnalysisStatisticRepository.findBySessionId(sessionId);
        return sessionRepository.findById(sessionId)
                .map(session -> sessionMapper.mapToSessionDetails(session, transcription, statistic))
                .orElseGet(() -> new SessionDetailsDTO());
    }

    private Session createSession(User user) {
        Session session = new Session();
        session.setUser(user);
        session.setPersons(randomPersons());
        sessionRepository.save(session);
        return session;
    }

    private Integer randomPersons() {
        return RANDOM.nextInt(18)+2;
    }

    private void createTranscription(Session session) {
        Transcription transcription = new Transcription();
        transcription.setSession(session);
        transcriptionRepository.save(transcription);
    }

    private void createImageStatistics(Session session) {
        ImageAnalysisStatistic statistic = new ImageAnalysisStatistic();
        statistic.setSession(session);
        imageAnalysisStatisticRepository.save(statistic);
    }

    private void updateSession(Long sessionId) {
        sessionRepository.updateLastUpdate(LocalDateTime.now(), sessionId);
    }

    private void updateSession(Session session, ImageAnalysisResponseDTO imageAnalysed) {
        session.setLastUpdate(LocalDateTime.now());
        session.setImage(imageAnalysed.getImage());
    }


}
