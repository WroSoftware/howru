package com.hackyeah.ruok.session.model.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class ScreenShotWithTranscriptionDTO {

    @JsonProperty("image")
    private String image;

    @JsonProperty("transcription")
    private String transcription;

}
