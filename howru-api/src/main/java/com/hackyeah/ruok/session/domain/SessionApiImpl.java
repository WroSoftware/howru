package com.hackyeah.ruok.session.domain;

import com.hackyeah.ruok.analysis.domain.AnalysisApi;
import com.hackyeah.ruok.analysis.model.dto.ImageAnalysisResponseDTO;
import com.hackyeah.ruok.analysis.model.dto.TextAnalysisResponseDTO;
import com.hackyeah.ruok.session.SessionApi;
import com.hackyeah.ruok.session.model.dto.ScreenShotWithTranscriptionDTO;
import com.hackyeah.ruok.session.model.dto.SessionDTO;
import com.hackyeah.ruok.session.model.entity.Session;
import com.hackyeah.ruok.user.model.dto.SessionForUserDTO;
import com.hackyeah.ruok.user.model.entity.User;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;

@Slf4j
@AllArgsConstructor
@Component
class SessionApiImpl implements SessionApi {

	private final SessionService sessionService;
	private final AnalysisApi analysisApi;

	@Override
	public SessionDTO createNewSession(User user) {
		return sessionService.createNewSession(user);
	}

	@Override
	public void addScreenShotWithTranscription(Long sessionId, ScreenShotWithTranscriptionDTO dto) {
		sessionService.addScreenShotWithTranscription(sessionId, dto);
		Session session = sessionService.getSession(sessionId);
		//TODO
//		ImageAnalysisResponseDTO imageAnalysis = analysisApi.analysePicture(dto.getImage());
//		sessionService.addImageAnalysis(session, imageAnalysis);
		TextAnalysisResponseDTO textAnalysis = analysisApi.analyseTranscription(dto.getTranscription());
		sessionService.addTextAnalysis(session, textAnalysis);
	}

	@Override
	public List<SessionForUserDTO> getSessionsForUser(Long userId) {
		return sessionService.getSessionsForUser(userId);
	}

	@Override
	public SessionDetailsDTO getSessionDetails(Long sessionId) {
		return sessionService.getSessionDetails(sessionId);
	}
}
