package com.hackyeah.ruok.session.domain;

import lombok.Builder;
import lombok.Value;

import java.math.BigDecimal;

@Value
@Builder
public class ImageStatisticsDTO {

    private BigDecimal angry;
    private BigDecimal scared;
    private BigDecimal happy;
    private BigDecimal sad;
    private BigDecimal neutral;

}
