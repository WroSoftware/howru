package com.hackyeah.ruok.session.domain;


import com.hackyeah.ruok.session.model.entity.TextAnalysis;
import org.springframework.data.jpa.repository.JpaRepository;

interface TextAnalysisRepository extends JpaRepository<TextAnalysis, Long> {
}
