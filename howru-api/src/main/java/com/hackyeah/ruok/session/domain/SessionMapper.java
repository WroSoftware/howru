package com.hackyeah.ruok.session.domain;

import com.hackyeah.ruok.session.model.dto.SessionDTO;
import com.hackyeah.ruok.session.model.entity.ImageAnalysisStatistic;
import com.hackyeah.ruok.session.model.entity.Session;
import com.hackyeah.ruok.session.model.entity.Transcription;
import com.hackyeah.ruok.user.model.dto.SessionForUserDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.Named;

import java.math.BigDecimal;
import java.math.RoundingMode;

@Mapper(componentModel = "spring")
interface SessionMapper {

    @Mapping(target = "name", source = "session")
    Session mapToEntity(SessionDTO sessionDTO);

    @Mapping(target = "session", source = "name")
    SessionDTO mapToDto(Session session);

    @Mappings({
        @Mapping(target = "start", source = "session.created"),
        @Mapping(target = "end", source = "session.lastUpdate"),
    })
    SessionForUserDTO mapToUserSession(Session session);

    @Mappings({
            @Mapping(target = "id", source = "session.id"),
            @Mapping(target = "start", source = "session.created"),
            @Mapping(target = "end", source = "session.lastUpdate"),
            @Mapping(target = "transcription", source = "transcription.transcription"),
            @Mapping(target = "statistics", source = "statistic", qualifiedByName = "mapStatistics")
    })
    SessionDetailsDTO mapToSessionDetails(Session session, Transcription transcription, ImageAnalysisStatistic statistic);


    @Named("mapStatistics")
    default ImageStatisticsDTO mapStatistics(ImageAnalysisStatistic statistic) {
        BigDecimal sum = new BigDecimal(statistic.getAngry()
                                        + statistic.getScared()
                                        + statistic.getHappy()
                                        + statistic.getSad()
                                        + statistic.getNeutral());
        if(sum.equals(BigDecimal.ZERO)){
            return ImageStatisticsDTO.builder()
                    .angry(BigDecimal.ZERO)
                    .scared(BigDecimal.ZERO)
                    .happy(BigDecimal.ZERO)
                    .sad(BigDecimal.ZERO)
                    .neutral(BigDecimal.ZERO)
                    .build();
        } else {
            return ImageStatisticsDTO.builder()
                    .angry(new BigDecimal(statistic.getAngry()).divide(sum, 2, RoundingMode.HALF_UP))
                    .scared(new BigDecimal(statistic.getScared()).divide(sum, 2, RoundingMode.HALF_UP))
                    .happy(new BigDecimal(statistic.getHappy()).divide(sum, 2, RoundingMode.HALF_UP))
                    .sad(new BigDecimal(statistic.getSad()).divide(sum, 2, RoundingMode.HALF_UP))
                    .neutral(new BigDecimal(statistic.getNeutral()).divide(sum, 2, RoundingMode.HALF_UP))
                    .build();
        }
    }
}
