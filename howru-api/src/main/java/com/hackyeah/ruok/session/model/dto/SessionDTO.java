package com.hackyeah.ruok.session.model.dto;

import lombok.Data;

@Data
public class SessionDTO {

    private String session;
    private Long id;
    
}
