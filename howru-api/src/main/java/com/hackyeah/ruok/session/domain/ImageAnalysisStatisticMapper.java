package com.hackyeah.ruok.session.domain;

import com.hackyeah.ruok.analysis.model.dto.ImageAnalysisResponseDTO;
import com.hackyeah.ruok.session.model.entity.ImageAnalysisStatistic;
import org.mapstruct.Mapper;

import java.util.Optional;

@Mapper(componentModel = "spring")
interface ImageAnalysisStatisticMapper {


    default void recalculate(ImageAnalysisStatistic statistic, ImageAnalysisResponseDTO imageAnalysed) {
        Optional.ofNullable(imageAnalysed)
                .map(ImageAnalysisResponseDTO::getEmotion)
                .ifPresent(emotion -> {
                    if ("angry".equals(emotion)) {
                        statistic.iAmAngry();
                    } else if ("scared".equals(emotion)) {
                        statistic.iAmScared();
                    } else if ("happy".equals(emotion)) {
                        statistic.iAmHappy();
                    } else if ("sad".equals(emotion)) {
                        statistic.iAmSad();
                    } else if ("neutral".equals(emotion)) {
                        statistic.iAmNeutral();
                    }

                });

    }

}
