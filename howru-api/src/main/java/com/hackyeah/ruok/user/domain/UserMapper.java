package com.hackyeah.ruok.user.domain;


import com.hackyeah.ruok.user.model.dto.UserDTO;
import com.hackyeah.ruok.user.model.entity.User;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import java.util.Random;

@Mapper(componentModel = "spring")
interface UserMapper {

    @Mapping(target = "status", source = "user", qualifiedByName = "calculateStatus")
    UserDTO mapToDto(User user);

    @Named("calculateStatus")
    default boolean calculateStatus(User user){
        return new Random(System.currentTimeMillis()).nextBoolean();
    }

}
