package com.hackyeah.ruok.user.model.dto;

import lombok.Data;

@Data
public class UserDTO {

    private Long id;
    private String name;
    private String surname;
    private String className;
    private String teacherName;
    private String teacherSurname;
    private boolean status;
}
