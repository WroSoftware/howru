package com.hackyeah.ruok.user.domain;

import com.hackyeah.ruok.session.model.dto.SessionDTO;
import com.hackyeah.ruok.user.model.dto.UserSessionDTO;
import com.hackyeah.ruok.user.model.entity.User;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper(componentModel = "spring")
interface UserSessionMapper {

    @Mappings({
        @Mapping(target = "userId", source = "user.id"),
        @Mapping(target = "sessionId", source = "session.id")
    })
    UserSessionDTO mapToUserSession(User user, SessionDTO session);
}
