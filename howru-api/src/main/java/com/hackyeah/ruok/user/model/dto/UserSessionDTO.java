package com.hackyeah.ruok.user.model.dto;

import lombok.Data;

@Data
public class UserSessionDTO {

    private Long userId;
    private Long sessionId;

}
