package com.hackyeah.ruok.user.domain;

import com.hackyeah.ruok.session.SessionApi;
import com.hackyeah.ruok.session.model.dto.SessionDTO;
import com.hackyeah.ruok.user.UserApi;
import com.hackyeah.ruok.user.model.dto.SessionForUserDTO;
import com.hackyeah.ruok.user.model.dto.UserDTO;
import com.hackyeah.ruok.user.model.dto.UserSessionDTO;
import com.hackyeah.ruok.user.model.entity.User;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@AllArgsConstructor
class UserApiImpl implements UserApi {

    private UserService userService;
    private SessionApi sessionApi;
    private UserSessionMapper userSessionMapper;

    @Override
    public UserSessionDTO createNewSessionByUserId(Long userId) {
        User user = userService.getUser(userId);
        SessionDTO newSession = sessionApi.createNewSession(user);
        return userSessionMapper.mapToUserSession(user, newSession);
    }

    @Override
    public List<UserDTO> getAllUsers() {
        return userService.getAllUsers();
    }

    @Override
    public List<SessionForUserDTO> getUserSessions(Long userId) {
        return sessionApi.getSessionsForUser(userId);
    }
}
