package com.hackyeah.ruok.user;

import com.hackyeah.ruok.user.model.dto.SessionForUserDTO;
import com.hackyeah.ruok.user.model.dto.UserDTO;
import com.hackyeah.ruok.user.model.dto.UserSessionDTO;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@AllArgsConstructor
@RestController
@RequestMapping("/user")
class UserController {

    private UserApi userApi;


    @GetMapping
    List<UserDTO> getAllUsers(){
        return userApi.getAllUsers();
    }

    @PostMapping("/{userId}/session")
    UserSessionDTO createNewSessionByUserId(@PathVariable("userId") Long userId) {
        return userApi.createNewSessionByUserId(userId);
    }

    @GetMapping("/{userId}/session")
    List<SessionForUserDTO> getUserSessions(@PathVariable("userId") Long userId) {
        return userApi.getUserSessions(userId);
    }

}
