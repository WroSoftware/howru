package com.hackyeah.ruok.user.model.entity;

import com.hackyeah.ruok.session.model.entity.Session;
import com.hackyeah.ruok.shared.model.entity.BaseEntity;
import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table(name = "app_user")
@SequenceGenerator(name = "gen", sequenceName = "user_gen", initialValue = 1, allocationSize = 1)
public class User  {

    @Id
    private Long id;

    private String name;
    private String surname;
    private String className;
    private String teacherName;
    private String teacherSurname;

    @OneToMany(mappedBy = "user")
    private List<Session> sessions;
}
