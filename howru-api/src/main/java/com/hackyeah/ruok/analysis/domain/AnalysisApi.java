package com.hackyeah.ruok.analysis.domain;

import com.hackyeah.ruok.analysis.model.dto.ImageAnalysisResponseDTO;
import com.hackyeah.ruok.analysis.model.dto.TextAnalysisResponseDTO;

public interface AnalysisApi {

    ImageAnalysisResponseDTO analysePicture(String image);
    TextAnalysisResponseDTO analyseTranscription(String transcription);
}
