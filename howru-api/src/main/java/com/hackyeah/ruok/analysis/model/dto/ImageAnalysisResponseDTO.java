package com.hackyeah.ruok.analysis.model.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.Optional;

@Data
public class ImageAnalysisResponseDTO {

    @JsonProperty("image")
    private String image;
    @JsonProperty("emotion_ex")
    private Labels labels;

    public String getEmotion() {
        return Optional.ofNullable(labels)
                .map(Labels::getEmotion)
                .orElse(null);
    }
}
