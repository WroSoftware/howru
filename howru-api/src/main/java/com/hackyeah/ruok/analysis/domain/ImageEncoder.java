package com.hackyeah.ruok.analysis.domain;

import java.util.Base64;

interface ImageEncoder {

    default byte[] decode(String encoded) {
        return Base64.getDecoder().decode(encoded);
    }
}
