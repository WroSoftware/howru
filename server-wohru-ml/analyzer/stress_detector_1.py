from scipy.spatial import distance as dist
from imutils.video import VideoStream
from imutils import face_utils
import numpy as np
import imutils
import time
import dlib
import cv2
from tensorflow.keras.preprocessing.image import img_to_array
from tensorflow.keras.models import load_model
import os

path = os.path.dirname(os.path.realpath(__file__))


def eye_brow_distance(leye, reye):
    global points
    distq = dist.euclidean(leye, reye)
    points.append(int(distq))
    return distq


def emotion_finder(faces, frame):
    global emotion_classifier
    EMOTIONS = ["angry", "disgust", "scared", "happy", "sad", "surprised", "neutral"]
    x, y, w, h = face_utils.rect_to_bb(faces)
    frame = frame[y : y + h, x : x + w]
    roi = cv2.resize(frame, (64, 64))
    roi = roi.astype("float") / 255.0
    roi = img_to_array(roi)
    roi = np.expand_dims(roi, axis=0)
    preds = emotion_classifier.predict(roi)[0]
    emotion_probability = np.max(preds)
    label_ext = EMOTIONS[preds.argmax()]
    if label_ext in ["scared", "sad"]:
        label = "stressed"
    else:
        label = "not stressed"
    return label_ext, label


def normalize_values(points, disp):

    normalized_value = abs(disp - np.min(points)) / abs(np.max(points) - np.min(points))
    stress_value = np.exp(-(normalized_value))
    print(stress_value)
    if stress_value >= 75:
        return stress_value, "High Stress"
    else:
        return stress_value, "low_stress"


predictor_path = os.path.join(path, "stress_detector_1", "shape_predictor_68_face_landmarks.dat")
emotion_classifier_path = os.path.join(path, "stress_detector_1", "_mini_XCEPTION.102-0.66.hdf5")
detector = dlib.get_frontal_face_detector()
predictor = dlib.shape_predictor(predictor_path)
emotion_classifier = load_model(emotion_classifier_path, compile=False)

points = []


def analyze_stress_detector(frame):

    global points

    points = []

    frame = cv2.flip(frame, 1)
    frame = imutils.resize(frame, width=500, height=500)

    (lBegin, lEnd) = face_utils.FACIAL_LANDMARKS_IDXS["right_eyebrow"]
    (rBegin, rEnd) = face_utils.FACIAL_LANDMARKS_IDXS["left_eyebrow"]

    # preprocessing the image
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    labels = []
    detections = detector(gray, 0)

    for detection in detections:
        emotion_ex, emotion = emotion_finder(detection, gray)
        cv2.putText(frame, "STATE: " + emotion_ex.upper(), (10, 35), cv2.FONT_HERSHEY_SIMPLEX, 1.0, (255, 0, 0), 2)
        shape = predictor(frame, detection)
        shape = face_utils.shape_to_np(shape)

        leyebrow = shape[lBegin:lEnd]
        reyebrow = shape[rBegin:rEnd]

        reyebrowhull = cv2.convexHull(reyebrow)
        leyebrowhull = cv2.convexHull(leyebrow)

        cv2.drawContours(frame, [reyebrowhull], -1, (0, 255, 0), 1)
        cv2.drawContours(frame, [leyebrowhull], -1, (0, 255, 0), 1)

        distq = eye_brow_distance(leyebrow[-1], reyebrow[0])
        stress_value, stress_label = normalize_values(points, distq)
        labels.append({"stress_level" : int(stress_value * 100), "stress" : stress_label, "emotion_ex" : emotion_ex, "emotion" : emotion})

    return frame, labels


# plt.plot(range(len(points)), points, "ro")
# plt.title("Stress Levels")
# plt.show()
