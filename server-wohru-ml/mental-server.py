import flask
from flask import jsonify
from flask import request
from PIL import Image
from PIL import ImageFont
from PIL import ImageDraw
from io import BytesIO
from flask import send_file

from analyzer import stress_detector_1

import cv2
import os
import random
import base64
import json

from sklearn.feature_extraction.text import TfidfVectorizer
import pandas as pd
from sklearn.model_selection import train_test_split
import spacy
from sklearn.naive_bayes import MultinomialNB
import pandas as pd
from sklearn.metrics import classification_report

app = flask.Flask(__name__, static_url_path="/static")
app.config["DEBUG"] = True

base_dir = os.path.dirname(os.path.realpath(__file__))
path_cache = os.path.join(base_dir, "cache")

nlp_pl = spacy.load("pl_core_news_md")
nlp_en = spacy.load("en_core_web_md")

clf_pl = None
vectorizer_pl = None

clf_en = None
vectorizer_en = None

def clean_lemma_en(s):

    doc = nlp_en(s)
    words = []

    for token in doc:
        if token.is_alpha and not token.is_stop:
            words.append(token.lemma_)

    return " ".join(words)

def clean_lemma_pl(s):

    doc = nlp_pl(s)
    words = []

    for token in doc:
        if token.is_alpha and not token.is_stop:
            words.append(token.lemma_)

    return " ".join(words)

def train_clf_pl():

    global clf_pl
    global vectorizer_pl

    df = pd.read_csv(os.path.join(base_dir,"pl.csv"))
    X = df["Text"].apply(clean_lemma_pl)
    y = df["Label"]

    X_train, X_test, y_train, y_test = train_test_split(X, y)

    vectorizer = TfidfVectorizer()
    X_train = vectorizer.fit_transform(X_train)
    X_test = vectorizer.transform(X_test)

    clf = MultinomialNB().fit(X_train, y_train)
    y_pred = clf.predict(X_test)
    print(classification_report(y_test, y_pred))
    clf_pl = clf
    vectorizer_pl = vectorizer

    return

def train_clf_en():

    global clf_en
    global vectorizer_en

    df = pd.read_csv(os.path.join(base_dir,"en.csv"))
    X = df["Text"].apply(clean_lemma_en)
    y = df["Label"]

    X_train, X_test, y_train, y_test = train_test_split(X, y)

    vectorizer = TfidfVectorizer()
    X_train = vectorizer.fit_transform(X_train)
    X_test = vectorizer.transform(X_test)

    clf = MultinomialNB().fit(X_train, y_train)
    y_pred = clf.predict(X_test)
    print(classification_report(y_test, y_pred))
    clf_en = clf
    vectorizer_en = vectorizer

    return


train_clf_pl()
train_clf_en()

def predict_pl(txt):

    global clf_pl
    global vectorizer_pl

    txt = clean_lemma_pl(txt)
    X_txt = vectorizer_pl.transform([txt])

    y_pred_txt = clf_pl.predict(X_txt)
    y_probs = clf_pl.predict_proba(X_txt)

    print(y_probs)
    return y_pred_txt

def predict_en(txt):

    global clf_en
    global vectorizer_en

    txt = clean_lemma_en(txt)
    X_txt = vectorizer_en.transform([txt])

    y_pred_txt = clf_en.predict(X_txt)
    y_probs = clf_en.predict_proba(X_txt)

    print(y_probs)
    return y_pred_txt

def serve_pil_image(pil_img):
    img_io = BytesIO()
    pil_img.save(img_io, "JPEG")
    img_io.seek(0)
    return send_file(img_io, mimetype="image/jpeg")

def return_image(pil_img, rt, labels):

    if rt == 0:
        return serve_pil_image(pil_img)

    img_io = BytesIO()
    pil_img.save(img_io, "JPEG")
    img_io.seek(0)

    b64 = base64.b64encode(img_io.getvalue())
    img = {"image": b64.decode('utf-8'), "labels" : labels}

    return jsonify(img), 200, {"Content-Type": "application/json; charset=utf-8"}

@app.route("/api/v1/analyzer/text/<lang>/", methods=["POST"])
def process_text(lang):

    txt = request.json["txt"]
    
    try:
        if lang.lower() == "pl":
            p = predict_pl(txt)
        else:
            p = predict_en(txt)
        p0 = p[0]
    except:
        p0 = "UNKNOWN"

    result = {"emotion" : p0, "txt": txt}

    return jsonify(result), 200, {"Content-Type": "application/json; charset=utf-8"}

@app.route("/api/v1/analyzer/image/base64/<rt>/", methods=["POST"])
def process_image_base64(rt):

    try:
        b64 = str(request.json["image"])
    except:
        print("Empty image request")

    i = random.randint(1, 1000000)
    file_name = os.path.join(path_cache, str(i) + ".jpg")
    file_name_labeled = os.path.join(path_cache, str(i) + "_labeled.jpg")
    print(file_name)

    with open(file_name,"wb") as f:
        f.write(base64.b64decode(b64))

    image_cv = cv2.imread(file_name)

    img_labeled, labels = stress_detector_1.analyze_stress_detector(image_cv)
    cv2.imwrite(file_name_labeled, img_labeled)
    return return_image(Image.open(file_name_labeled), int(rt), labels)


@app.route("/api/v1/analyzer/image/<rt>/", methods=["POST"])
def process_image(rt):

    i = random.randint(1, 1000000)
    file_name = os.path.join(path_cache, str(i) + ".jpg")
    file_name_labeled = os.path.join(path_cache, str(i) + "_labeled.jpg")

    file = request.files["image"]
    img = Image.open(file.stream)
    img.save(file_name)
    image_cv = cv2.imread(file_name)

    img_labeled, labels = stress_detector_1.analyze_stress_detector(image_cv)
    cv2.imwrite(file_name_labeled, img_labeled)
    return return_image(Image.open(file_name_labeled), int(rt), labels)


app.run(host="0.0.0.0", port=8321)
