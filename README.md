# howRU

How Are You application


## howRU machine learning

It is a server application responsible for analyzing the video stream and transcription in terms of emotions. The application is based on python.

## howRU ui

It is a client application. Here, the camera image and audio are captured and sent to the server for analysis.

Work took place in the repository
- [ ] [MTE] (https://github.com/dziadyk-m/MTE)


## howRu api

The server application with which the client part contacts. Responsible for communication with machine learning and database entries as well as calculating statistics.

Work took place in the repository
- [ ] [rouk](https://gitlab.com/WroSoftware/ruok)
